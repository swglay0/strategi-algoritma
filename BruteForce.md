# UAS NO 3

Python

Source Code : 

    def find_optimal_stops(stops, target_distance, rest_distance):
    optimal_stops = []
    last_stop = 0
    
    for i in range(len(stops)):
        if stops[i] - last_stop > rest_distance:
            optimal_stops.append(stops[i-1])
            last_stop = stops[i-1]
    
    optimal_stops.append(stops[-1])
    return optimal_stops

    stops = [10, 25, 30, 40, 50, 75, 80, 110, 130]

    target_distance = 140

    rest_distance = 30

    optimal_stops = find_optimal_stops(stops, target_distance, rest_distance)

    print("Titik-titik perhentian optimal:")
    for stop in optimal_stops:
        print(stop, "km")

Output : 
![](https://gitlab.com/swglay0/strategi-algoritma/-/raw/main/Screenshot_1.png)


